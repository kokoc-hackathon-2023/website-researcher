package main

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"log"
	"net/http"
	"strings"
)

func main() {

	url := "https://street-beat.ru/"
	res, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		log.Fatalf("status code error: %d %s", res.StatusCode, res.Status)
	}

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Fatal(err)
	}

	result := doc.Text()
	result = strings.TrimSpace(result)

	fmt.Println(result)

	doc.Find("script").Each(func(i int, el *goquery.Selection) {
		el.Remove()
	})
	doc.Find("iframe").Each(func(i int, el *goquery.Selection) {
		el.Remove()
	})

	// Links:FooBarBazTEXT I WANT

	//fmt.Println(doc.Text())
}
