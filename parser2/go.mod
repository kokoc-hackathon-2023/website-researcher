module parser2

go 1.21.1

require (
	github.com/PuerkitoBio/goquery v1.8.1 // indirect
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/blang/semver v3.5.1+incompatible // indirect
	github.com/tebeka/selenium v0.9.9 // indirect
	golang.org/x/net v0.7.0 // indirect
)
