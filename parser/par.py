


# from selenium import webdriver
# from selenium.webdriver.common.desired_capabilities import desiredcapabilities
#
# driver = webdriver.remote("http://127.0.0.1:4444/wd/hub", desiredcapabilities.chrome)


import datetime
from selenium import webdriver


# -----
options = webdriver.FirefoxOptions()

options.add_argument("--headless")
options.add_argument('--disable-gpu')
options.add_argument('--disable-dev-shm-usage')
options.add_argument("--window-size=1920,1080")


profile = webdriver.FirefoxProfile()
profile.set_preference('permissions.default.image', 2)
profile.set_preference('permissions.default.stylesheet', 2)

options.profile = profile



#  ----

# options = webdriver.ChromeOptions()
#
# # options.add_argument('--no-sandbox')
# # options.add_argument('--disable-dev-shm-usage')
# # options.add_argument('--disable-gpu')
# # options.add_argument('--headless')
# # options.add_argument('--start-maximized')
#
# options.add_argument("--no-sandbox")
# options.add_argument("--headless")
# options.add_argument("--disable-gpu")


# chrome_options.set_capability("browserVersion", "67")
# chrome_options.set_capability("platformName", "Windows XP")
driver = webdriver.Remote(
    command_executor='http://127.0.0.1:4444/wd/hub',
    options=options
)

start_time = datetime.datetime.now()


driver.get("https://www.digitalocean.com/community/tutorials/how-to-make-a-web-application-using-flask-in-python-3")
print("title", driver.title)


delta_time = datetime.datetime.now() - start_time
print("TIME: ", delta_time.seconds, "ms")


driver.quit()

# ---------

# import os
# from flask import Flask
# from selenium import webdriver
#
# app = Flask(__name__)
#
#
# @app.route("/")
# def parse():
#     return "<h1 style='color:blue'>Hello There!</h1>"
#
#
# class Parser():
#
#     def __init__(self, selenium_url):
#         options = webdriver.FirefoxOptions()
#         # options.add_argument('--no-sandbox')
#         options.add_argument('--headless')
#         options.add_argument('--disable-gpu')
#         options.add_argument('--disable-dev-shm-usage')
#         options.add_argument("--window-size=1920,1080")
#
#         self.driver = webdriver.Remote(
#             command_executor=selenium_url,
#             options=options
#         )
#         self.driver.implicitly_wait(10)
#
#     def tearDown(self):
#         """Stop web driver"""
#         self.driver.quit()
#
#     def parse(self, url):
#         self.driver.get(url)
#         print("title", self.driver.title)
#
#
# if __name__ == "__main__":
#     app_port = os.getenv('APP_PORT')
#     if app_port == 0:
#         app_port = 4000
#
#     selenium_url = os.getenv("SELENIUM_URL")
#     if selenium_url is None:
#         selenium_url = 'http://127.0.0.1:4444/wd/hub'
#
#     print("kke")
#
#     parser = Parser(selenium_url)
#
#     print("kke1")
#
#     domain = "https://street-beat.ru/"
#
#     parser.parse(domain)
#
#     print("kke2")
#
#     # 'http://127.0.0.1:4444/wd/hub'
#
#     app.run(host='0.0.0.0', port=app_port)
