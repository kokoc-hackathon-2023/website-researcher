создаем окружение
* python -m venv parser-venv

заходим в окружение
* source parser-venv/bin/activate

устанавливаем пакеты
* pip install -r requirements.txt

Если были установлены пакеты, то для сохранения их для потомков используем:
* pip freeze > requirements.txt

---

запуск
* python parser.py