# import time
#
# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.common.by import By
# from selenium.webdriver.firefox.options import Options
#
# options = Options()
# options.add_argument("--headless")
# driver = webdriver.Firefox(options)
#
# start_time = time.process_time()
#
#
# domain = "https://street-beat.ru/"
#
# driver.get(domain)
#
# page_raw = driver.page_source
#
#
# # print(page_raw)
#
# print("title", driver.title)
#
# print(driver.find_element(By.XPATH, "/html/body").text)
#
#
# print("TIME: ", (time.process_time() - start_time) * 1000, "ms")
# driver.close()


# import time
# import os
#
# from selenium import webdriver
# from selenium.webdriver.chrome.options import Options
# from selenium.webdriver.chrome.service import Service
# from selenium.webdriver.common.by import By
#
# def test_fullpage_screenshot(url: str, filename: str):
#     options = Options()
#     options.add_argument('--no-sandbox')
#     options.add_argument('--disable-gpu')
#     options.add_argument('--disable-dev-shm-usage')
#     options.add_argument('--headless')
#     options.add_argument('--start-maximized')
#
#     service = Service(executable_path="./chromedriver")
#
#     driver = webdriver.Chrome(service=service,
#                               options=options)
#     try:
#         driver.get(url)
#         time.sleep(2)
#
#         ele = driver.find_element(By.TAG_NAME, 'body')
#         height = driver.execute_script("return document.body.scrollHeight")
#         driver.set_window_size(1920, height)
#
#         if not os.path.exists("screenshots"):
#             os.mkdir("screenshots")
#
#         driver.save_screenshot(f"screenshots/{filename}")
#     except Exception:
#         pass
#     finally:
#         driver.close()
#         driver.quit()
#
# if __name__ == "__main__":
#     test_fullpage_screenshot("https://tproger.ru", "tproger.png")
#     test_fullpage_screenshot("https://stackoverflow.com", "stackoverflow.png")


from flask import Flask
from selenium import webdriver

app = Flask(__name__)


@app.route("/")
def parse():
    return "<h1 style='color:blue'>Hello There!</h1>"


class Parser():

    def setUp(self):
        """Start web driver"""
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        chrome_options.add_argument("--window-size=1920,1080")
        self.driver = webdriver.Chrome(options=chrome_options)
        self.driver.implicitly_wait(10)

    def tearDown(self):
        """Stop web driver"""
        self.driver.quit()

    def parse(self, url):
        self.driver.get(url)
        print("title", self.driver.title)


if __name__ == "__main__":
    # parser = Parser()
    # parser.setUp()
    #
    # domain = "https://street-beat.ru/"
    #
    # parser.parse(domain)

    app.run(host='0.0.0.0', port=4000)
