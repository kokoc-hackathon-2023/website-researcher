package main

import (
	"github.com/labstack/echo/v4"
	"log"
	"net/http"
	"website-reseacher/src/handler"
)

func main() {
	e := echo.New()

	handl := new(handler.Handler)

	e.Add(http.MethodPost, "/api/v1/research", handl.Research)

	log.Fatal(e.Start(":80"))
}
