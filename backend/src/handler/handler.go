package handler

import (
	"net/http"

	"github.com/labstack/echo/v4"

	"website-reseacher/src/models"
	"website-reseacher/src/service"
)

type Handler struct {
}

func (h *Handler) Research(ctx echo.Context) error {
	reqBody := models.RequestBody{}

	if err := ctx.Bind(&reqBody); err != nil {
		return ctx.JSON(http.StatusInternalServerError, "1")
	}

	result := service.Calc(reqBody.Website)

	return ctx.JSON(http.StatusOK, result)
}
