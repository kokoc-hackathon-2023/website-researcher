package models

type Result struct {
	ErrorMessage string   `json:"error_message"`
	Topics       []string `json:"topics"`
}

type RequestBody struct {
	Website string `json:"website"`
}
